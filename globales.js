let baseMLabURL = "https://api.mlab.com/api/1/databases/techubduruguay/collections/";
let apikeyMLab = "apiKey=4QjIOXHIHylWiKsqAIzd6U4C1c6AFill";
let meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
let diasSemana = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");

//export {baseMLabURL, apikeyMLab, meses, diasSemana};

module.exports = {
    baseMLabURL: baseMLabURL,
    apikeyMLab: apikeyMLab,
    meses: meses,
    diasSemana: diasSemana
}
