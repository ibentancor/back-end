var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var requestJSON = require('request-json');

var baseMLabURL = "https://api.mlab.com/api/1/databases/techubduruguay/collections/";

var apikeyMLab = "apiKey=4QjIOXHIHylWiKsqAIzd6U4C1c6AFill";

var app = express();
var port = process.env.PORT || 3000;
const URI = '/api-uruguay/v1/';
var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
var diasSemana = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
var hoy = new Date();
var userFile = require('./users.json');
var ctaFile = require('./cuentas.json');
app.use(bodyParser.json());



app.listen(port);
console.log('Escuchando en el puerto -->' + port + '<--');


//GET saludo
app.get(URI,
  function(req, res) {
    res.send('Hola API desde Node, hoy es ' + hoy.getDate() + '/' + meses[hoy.getMonth()] + '/' + hoy.getFullYear());
  }
);




// GET users consumiendo API REST de mLab
app.get(URI + 'users',
  function(req, res) {
    console.log("GET /api-uruguay/v1/users");
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if (err) {
          response = {
            "msg": "Error obteniendo usuario."
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {
              "msg": "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
  });




// Petición GET con id en mLab
app.get(URI + 'users/:id',
  function(req, res) {
    console.log("GET /api-uruguay/v1/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body) {
        console.log("Respuesta mLab correcta.");
        //  var respuesta = body[0];
        var response = {};
        if (err) {
          response = {
            "msg": "Error obteniendo usuario."
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {
              "msg": "Usuario no encontrado.",
              body
            }
            res.status(404);
          }
        }
        res.send(response);
      });
  });


// POST 'users' mLab
app.post(URI + 'users',
  function(req, res) {
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + apikeyMLab,
      function(error, respuestaMLab, body) {
        newID = body.length + 1;
        console.log("newID:" + newID);
        var newUser = {
          "id": newID + 1,
          "first_name": req.body.first_name,
          "last_name": req.body.last_name,
          "email": req.body.email,
          "password": req.body.password
        };
        clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
          function(error, respuestaMLab, body) {
            res.send(body);
          });
      });
  });

// PUT 'users' mLab
app.put(URI + 'users/:id',
  function(req, res) {
    var clienteMlab = requestJSON.createClient(baseMLabURL);

    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    clienteMlab.get('user?' + queryString + apikeyMLab, function(error, respuestaMLab, body) {
      //        console.log("borrar", body);
      var respuesta = {};
      if (error) {
        respuesta = {
          "msg": "Error obteniendo usuario."
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          respuesta = body;
          console.log(body[0]._id.$oid);
          var actUser = {
            "id": body[0].id,
            "first_name": req.body.first_name,
            "last_name": req.body.last_name,
            "email": req.body.email,
            "password": req.body.password
          };
          clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, actUser, function(error1, respuestaMLab1, body1) {
            if (error1) {
              console.log("Error al actualizar", body1);
            } else {
              console.log("Actualiza sin error", actUser, body1);
              respuesta = body1;
            }
          });
        } else {
          respuesta = {"msg": "Usuario no encontrado.", body1};
          res.status(404);
        }
      }
//      respuesta = body1;
      res.send(respuesta);
    });
  });

// DELETE 'users' mLab
app.delete(URI + 'users/:id',
  function(req, res) {
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    clienteMlab.get('user?' + queryString + apikeyMLab, function(error, respuestaMLab, body) {
      //        console.log("borrar", body);
      var respuesta = {};
      if (error) {
        respuesta = {
          "msg": "Error obteniendo usuario."
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          respuesta = body;
          console.log(body[0]._id.$oid);
          clienteMlab.delete('user/' + body[0]._id.$oid + '?' + apikeyMLab, function(error1, respuestaMLab1, body1) {
            if (error1) {
              console.log("Error al borrar", body1);
            } else {
              console.log("Borra sin error", body1);
            }

          });
        } else {
          respuesta = {
            "msg": "Usuario no encontrado.",
            body
          };
          res.status(404);
        }
      }
      res.send(respuesta);
    });

  });
