var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var requestJSON = require('request-json');

var globales = require('./globales');

var app = express();
var port = process.env.PORT || 3000;
const URI = '/api-uruguay/v1/';
var hoy = new Date();

//import {baseMLabURL, apikeyMLab, meses, diasSemana} from 'globales';
var meses = globales.meses;
var baseMLabURL = globales.baseMLabURL;
var apikeyMLab = globales.apikeyMLab;
var diasSemana = globales.diasSemana;


app.use(bodyParser.json());

app.listen(port);
console.log('Escuchando en el puerto -->' + port + '<--');


//GET saludo
app.get(URI,
  function(req, res) {
    res.send('API desde Node.js, hoy es ' + diasSemana[hoy.getDay()] + ', '+ hoy.getDate() + ' de ' + meses[hoy.getMonth()] + ' de ' + hoy.getFullYear());
  }
);




// GET users consumiendo API REST de mLab
app.get(URI + 'users',
  function(req, res) {
    console.log("GET /api-uruguay/v1/users");
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if (err) {
          response = {"msg": "Error obteniendo usuario."}
          res.status(500);
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {"msg": "Usuario no encontrado."}
            res.status(404);
          }
        }
        res.send(response);
      });
  });




// Petición GET con id en mLab
app.get(URI + 'users/:id',
  function(req, res) {
    console.log("GET /api-uruguay/v1/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body) {
        console.log("Respuesta mLab correcta.");
        //  var respuesta = body[0];
        var response = {};
        if (err) {
          response = {"msg": "Error obteniendo usuario."}
          res.status(500);
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {"msg": "Usuario no encontrado.", body}
            res.status(404);
          }
        }
        res.send(response);
      });
  });


// POST 'users' mLab
app.post(URI + 'users',
  function(req, res) {
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + apikeyMLab,
      function(error, respuestaMLab, body) {
        newID = body.length + 1;
        console.log("newID:" + newID);
        var newUser = {
          "id": newID + 1,
          "first_name": req.body.first_name,
          "last_name": req.body.last_name,
          "email": req.body.email,
          "password": req.body.password
        };
        clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
          function(error, respuestaMLab, body) {
            res.send(body);
          });
      });
  });

// PUT 'users' mLab
app.put(URI + 'users/:id',
  function(req, res) {
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var respuesta = {};

    clienteMlab.get('user?' + queryString + apikeyMLab, function(error, respuestaMLab, body) {
      //console.log("borrar", body);
      //var respuesta = {};
      if (error) {
        respuesta = {"msg": "Error obteniendo usuario."};
        res.status(500);
      } else {
        if (body.length > 0) {
          var id = {"id": body[0].id};
          var nom = {};
          var ape = {};
          var mail = {};
          var pass = {};
          var modUser = {};
          if (req.body.first_name != undefined) {nom = {"first_name": req.body.first_name};} else {nom = {"first_name": body[0].first_name};}
          if (req.body.last_name != undefined) {ape = {"last_name": req.body.last_name};} else {ape = {"last_name": body[0].last_name};}
          if (req.body.email != undefined) {mail = {"email": req.body.email};} else {mail = {"email": body[0].email};}
          if (req.body.password != undefined) {pass = {"password": req.body.password};} else {pass = {"password": body[0].password};}
          Object.assign(modUser, id, nom, ape, mail, pass);
          console.log("respuesta -> " ,body[0]._id.$oid);
          console.log("para modificar -> ",modUser);
          console.log("las 5 -> ", id, nom, ape, mail, pass);
            clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, modUser, function(error1, respuestaMLab1, body1) {
              console.log("respuesta -> ", body1);
              respuesta = JSON.parse(JSON.stringify(body1));
            });
        } else {
          respuesta = {"msg": "Usuario no encontrado.", body};
          res.status(404);
        }
      }

  });
  res.send(respuesta);
});

// DELETE 'users' mLab
app.delete(URI + 'users/:id',
  function(req, res) {
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    clienteMlab.get('user?' + queryString + apikeyMLab, function(error, respuestaMLab, body) {
      //        console.log("borrar", body);
      var respuesta = {};
      if (error) {
        respuesta = {"msg": "Error obteniendo usuario."};
        res.status(500);
      } else {
        if (body.length > 0) {
          respuesta = body;
          console.log(body[0]._id.$oid);
          clienteMlab.delete('user/' + body[0]._id.$oid + '?' + apikeyMLab, function(error1, respuestaMLab1, body1) {
            if (error1) {
              console.log("Error al borrar", body1);
              respuesta = {"msg": "Error al borrar. ", body1};
            } else {
              console.log("Borra sin error", body1);
              respuesta = {"msg": "Borrado correctamente. ", body1};
            }
          });
        } else {
          respuesta = {"msg": "Usuario no encontrado.", body};
          res.status(404);
        }
      }
      res.send(respuesta);
    });
});

// LOGIN 'users' mLab
app.post(URI + 'login',
  function(req, res) {
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var respCli = {};
    var email = req.body.email;
    var pass = req.body.password;
    console.log("Login -> ", email , pass);
    var qsemail = 'q={"email": "' + email + '"}&';
    var qspass = 'q={"password": "' + pass + '"}&';
    console.log("Params-> ", qsemail, qspass);
    clienteMlab.get('user?' + qsemail + apikeyMLab,
        function(error, respuestaMLab, body) {
          if (!error){
            var respuesta = body[0];
            console.log("Respuesta:", respuesta);
            if (respuesta != undefined){
              if (respuesta.password == pass){
                var sesion = {"logged": true};
                var login = '{"$set":' + JSON.stringify(sesion) + '}';
                console.log("Cadena set -> ", login);
                clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(login),
                function (error1, respuestaMLab1, body1){
                  if (!error1){
                      respCli = {"msg": "Login correcto..."};
                      console.log("Login correcto-> ", body1, "respCli -> ", respCli);
                      res.status(200);
                      res.send(respCli);
                  } else {
                      respCli = {"msg": "Error al registrar el login..."};
                      res.status(500);
                      res.send(respCli);
                  }
                });
              } else {
                console.log("Contraseña incorrecta...");
                respCli = {"msg": "Contraseña incorrecta"};
                res.status(401);
                res.send(respCli);
              }
            }else {
              console.log("Usuario incorrecto...");
              respCli = {"msg": "Usuario incorrecto"};
              res.status(404);
              res.send(respCli);
            }
          }else {
            respCli = {"msg": "Error al consultar usuario..."};
            res.status(500);
            res.send(respCli);
          }
    });
    console.log("respCli -> ", respCli);
});

// LOGOUT 'users' mLab
app.post(URI + 'logout',
  function(req, res) {
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var respCli = {};
    var email = req.body.email;
    console.log("Logout -> ", email);
    var qsemail = 'q={"email": "' + email + '"}&';
    console.log("Params-> ", qsemail);

    clienteMlab.get('user?' + qsemail + apikeyMLab,
        function(error, respuestaMLab, body) {
          if (!error){
            var respuesta = body[0];
            console.log("Respuesta:", respuesta);
            if (respuesta != undefined){
              if (respuesta.logged != undefined){
                var sesion = {"logged": true};
                var login = '{"$unset":' + JSON.stringify(sesion) + '}';
                console.log("Cadena set -> ", login);
                clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(login),
                function (error1, respuestaMLab1, body1){
                  if (!error1){
                      respCli = {"msg": "Logout correcto..."};
                      console.log("Logout correcto-> ", body1, "respCli -> ", respCli);
                      res.status(200);
                      res.send(respCli);
                  } else {
                      respCli = {"msg": "Error al registrar el logout..."};
                      res.status(500);
                      res.send(respCli);
                  }
                });
              } else {
                console.log("Usuario no logueado...");
                respCli = {"msg": "Usuario no logueado"};
                res.status(401);
                res.send(respCli);
              }
            }else {
              console.log("Usuario incorrecto...");
              respCli = {"msg": "Usuario incorrecto"};
              res.status(404);
              res.send(respCli);
            }
          }else {
            respCli = {"msg": "Error al consultar usuario..."};
            res.status(500);
            res.send(respCli);
          }
    });
    console.log("respCli -> ", respCli);
});
